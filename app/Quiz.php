<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quiz extends Model
{

    protected $table = 'quizes';

    protected $fillable = [
        'type', 'options','answer','points','test_id','case_sensitive','exercise_number'
    ];

    public function test()
    {
        return $this->belongsTo(Test::class);
    }


    public static function rules($update = false)
    {
        return [];
    }

}
