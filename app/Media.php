<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    protected $fillable = [
        "title",
        "file"
    ];



    public function getFileAttribute($value)
    {
        return asset('storage/media/'.$value);
    }

    public function setFileAttribute($value)
    {
        $file = uniqid('media_', true) . '.' . $value->extension();
        $value->storeAs('public/media', $file);
        $this->attributes['file'] = $file;
    }

    public static function rules()
    {
        return [
            "title" => "required",
            "file" => "file|required",
        ];
    }
}
