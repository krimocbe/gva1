<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    //
        protected $fillable = [
        'name','duration','language','body_question','num_exercises'
    ];


    public function languageCode()
    {
    	switch ($this->attributes['language']) {
    		case 0:
    			return 'FR';
    		case  1:
    		    return 'EN';
			case  2:
			    return 'DE';
		}
    }




    /*----------  Relations  ----------*/

    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    public function quizes()
    {
    	return $this->hasMany('App\Quiz');
    }


    /*----------  Rules  ----------*/

    public static function rules($update = false)
    {
        $default = [
            "name" => "required|string",
            "language" => "required|integer",
            "duration" => "required|integer",
            "num_exercises" => "required|integer",
        ];

        if($update) {
            $default['body_question'] = 'required';
        }

        return $default;

    }

}
