<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Test;
use App\Fig;
use App\Member;
use App\Member_quiz;
use App\Quiz;
use View;


class TestsController extends Controller
{
    //
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $tests = Test::all();
        if (request()->wantsJson()) {
            return response()->json($tests);
                                    }
        // load the view and pass the tests
        return View::make('dashboard.tests.index')
            ->with('tests', $tests);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        
        return view('dashboard.tests.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request,Test::rules());
        $test = Test::create(array_merge($request->all(),['body_question' => '']));
        return redirect()->route('tests.edit',$test->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return Test::with('quizes')->findOrFail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $test = Test::find($id);

        return view('dashboard.tests.edit',compact('test'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request,Test $test)
    {
        $this->validate($request,Test::rules(true));
        $test->update($request->all());
        return redirect()->back()->withSuccess(trans('app.success_update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {

        Test::destroy($id);

        return redirect()->route('tests.index');
    }
}
