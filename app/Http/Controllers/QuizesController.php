<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Quiz;
use App\Test;

class QuizesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $quiz=Quiz::all();
        if (request()->wantsJson())
        {
            return response()->json($quiz);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,Test $test)
    {
        $this->validate($request,Quiz::rules());


        $data = [];

        switch ($request->type) {
            case 'tto':
                $data = [
                    "type" => "tto",
                    "answer" => $request->answer,
                    "points" => $request->points,
                    'case_sensitive' => $request->case_sensitive,
                    'exercise_number' => $request->exercise_number,
                ];
                break;
            case 'ttc':
                $data = [
                    "type" => "ttc",
                    "answer" => $request->answer['value'],
                    "points" => $request->points,
                    "options" => json_encode($request->options),
                    'exercise_number' => $request->exercise_number,
                ];
                break;
            case 'rl':
                $data = [
                    "type" => "rl",
                    "options" => $request->options,
                    'case_sensitive' => $request->case_sensitive,
                    'exercise_number' => $request->exercise_number,
                ];
                break;
            case 'rla':
                $data = [
                    "type" => "rla",
                    "answer" => $request->answer,
                    "points" => $request->points,
                    "options" => $request->options,
                    'exercise_number' => $request->exercise_number,
                ];
                break;
            case 'qcm':
                $data = [
                    "type" => "qcm",
                    "answer" => $request->answer['value'],
                    "points" => $request->points,
                    "options" => json_encode($request->options),
                    'exercise_number' => $request->exercise_number,
                ];
                break;
            default:
                return;
        }

        return $test->quizes()->create($data);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Test $test,Quiz $quiz)
    {

        $this->validate($request,Quiz::rules());

        $data = [];

        switch ($request->type) {
            case 'tto':
                $data = [
                    "answer" => $request->answer,
                    "points" => $request->points,
                    'case_sensitive' => $request->case_sensitive,
                    'exercise_number' => $request->exercise_number,
                ];
                break;
            case 'ttc':
                $data = [
                    "answer" => $request->answer['value'],
                    "points" => $request->points,
                    "options" => json_encode($request->options),
                    'exercise_number' => $request->exercise_number,
                ];
                break;
            case 'rl':
                $data = [
                    "options" => $request->options,
                    'case_sensitive' => $request->case_sensitive,
                    'exercise_number' => $request->exercise_number,
                ];
                break;
            case 'rla':
                $data = [
                    "answer" => $request->answer,
                    "points" => $request->points,
                    "options" => $request->options,
                    'exercise_number' => $request->exercise_number,
                ];
                break;
            case 'qcm':
                $data = [
                    "answer" => $request->answer['value'],
                    "points" => $request->points,
                    "options" => json_encode($request->options),
                    'exercise_number' => $request->exercise_number,
                ];
                break;
            default:
                return;
        }
        $quiz->update($data);
        return $quiz;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {

        Quiz::find($request->id)->delete();
        $quiz=Quiz::all();
        if (request()->wantsJson())
        {
            return response()->json($quiz);
        }
    }
}
