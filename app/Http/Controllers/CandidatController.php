<?php

namespace App\Http\Controllers;

use App\Quiz;
use App\Test;
use Illuminate\Http\Request;

class CandidatController extends Controller
{
    public function login()
    {
      return view('candidat.login');
    }

    public function handleLogin(Request $request)
    {
    	$userdata = array(
        'email'     => $request->email,
        'password'  => $request->password
    						);

        if (auth()->attempt($userdata)) {
            return redirect()->to('/candidat/tests');
    	} else {
            return redirect()->back()->with('error','Email/Mot de passe incorrect');
    	}
    }

    public function tests()
    {
        dd("handleLogino");
        $tests = auth()->user()->tests;
        return view('candidat.tests',compact('tests'));
    }


    public function test($id)
    {
        $user = auth()->user();
        $test = $user->tests()->findOrFail($id);

        $quizes = $test->quizes;

        $body = $test->body_question;
     
        
        foreach ($quizes as $q) {

            switch ($q->type) {
                case 'tto':
                    $input = '<input type="text" class="form-control" data-id="'. $q->id .'" required name="quiz-'. $q->id .'" />';
                    break;
                case 'ttc':
                    $options = json_decode($q->options,true);
                    $input = '<select required  class="form-control" name="quiz-'. $q->id .'" >';
                    foreach ($options as $option) {
                        $input .= '<option value="' . $option['value'] . '">' . $option['label'] .  '</option>';
                    }
                    $input .= '</select>';
                    break;
                case 'qcm':
                    $options = json_decode($q->options,true);
                    $input = '<div>';
                    foreach ($options as $option) {
                        $input .= '<div class="form-check"> <input class="form-check-input" name="quiz-' . $q->id . '" type="radio" value="'. $option['value'] .'"> <label class="form-check-label" for="defaultCheck1">'. $option['label'] .'</label> </div>';
                    }
                    $input .= '</div>';
                     break;
                case 'rl':
                    $input = '<textarea class="form-control rl"  name="quiz-'. $q->id .'">' . $q->options .'</textarea>';
                    break;
                case 'rla':
                    $input = '<textarea class="form-control"  name="quiz-'. $q->id .'">' . $q->options .'</textarea>';
                    break;

            }
            $body = str_replace('[*'. $q->id .']',$input,$body);
        }



        // PARSE ENDEX TAG
        $body = str_replace('<p>[*ENDEX]</p>', '</div>', $body);
        $body = str_replace('<p>[*STARTEX]</p>', '<div class="exercice hidden">', $body);


        // PARSE TEST BODY HERE FOR NOW


        return view('candidat.show_test',compact('user','test','body'));
    }

    public function postTest($id,Request $request)
    {

        // dd($request->all());
        $user_id = auth()->id();
        $test_user = \DB::table('test_user')->where('user_id',$user_id)->where('test_id',$id)->first();
        $quizes = Quiz::where('test_id', $id)->get();
        $validation = [];

        // foreach ($quizes as $q) {
        //     $validation['quiz-'.$q->id] = 'required';
        // }

        //$this->validate($request,$validation);


        // Create array of test_user_quiz
        $data = [];
        foreach ($quizes as $quiz) {
            $r = 'quiz-' . $quiz->id;

            $correct_answer = $quiz->answer;
            $user_answer = request($r);

            if(!$quiz->case_sensitive) {
                $correct_answer = strtolower($quiz->answer);
                $user_answer = strtolower($user_answer);
            }

            $point = (request()->has($r) && $user_answer == $correct_answer) ? $quiz->points : 0;


            $data[] = [
                'test_user_id' => $test_user->id, // ???
                'quiz_id'      => $quiz->id, // ???
                'points'       => $point,
                'answer'       => request($r)
            ];
        }

        // /Insert
        \DB::table('test_user_quiz')->insert($data);

        \DB::table('test_user')->where('user_id',$user_id)->where('test_id',$id)->update(['completed' => true]);

        // Return
        return redirect()->route('candidat.test_end',$id);
    }



    public function test_end()
    {
        return redirect()->route('candidat.tests')->with('success', true);
    }

}
