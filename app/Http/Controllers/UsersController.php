<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use App\Test;
use App\Quiz;
use DB;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $items = User::candidats()->with('roles')->get();

        if (request()->wantsJson()) {
            return response()->json($items);
        }

        return view('dashboard.users.index',compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, User::rules());
        $item = User::create($request->all());

        $item->attachRole(2);
        $item->tests()->attach($request->tests);
        return redirect()->route('users.index')->withSuccess(trans('app.success_store'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function tests($id)
    {
        $user = User::with('tests')->find($id);
        return view('dashboard.users.tests',compact('user'));
    }

    public function test($id,$test_id)
    {
        $user = User::findOrFail($id);
        
        
        $test = Test::with('quizes')->findOrFail($test_id);
        
        $test_user = DB::table('test_user')->where('user_id',$id)->where('test_id',$test_id)->first();


        $answers = DB::table('test_user_quiz')->where('test_user_id',$test_user->id)->get();
        

        $total_points = $answers->sum('points');
        
        $detailed_points =$test->quizes->groupBy('exercise_number')->map(function($v,$k) use ($answers) {
            return collect(["ex" => $k, 'real_points' => $v->sum('points') , "user_points" => $answers->whereIn('quiz_id',$v->pluck('id'))->sum('points')]);
        });
        
        $quizes = $test->quizes;
       
        $body = $test->body_question;
       

        foreach ($quizes as $q) {
            $answer = $answers->where('quiz_id',$q->id)->first();
            switch ($q->type) {
                case 'tto':

                    $input = '<input value="'. ($answer ? $answer->answer : "") .'" type="text" class="cand-res form-control disabled" disabled data-id="'. $q->id .'" name="quiz-'. $q->id .'" /> <span class="text-danger">( '. ( $answer ? $answer->points : '-' ) .' pt)</span>';
                    break;
                case 'ttc':
                    $options = json_decode($q->options,true);
                    $input = '<select disabled class="cand-res form-control disabled" name="quiz-'. $q->id .'" >';


                    foreach ($options as $option) {
                        // $input .= '<option  value="' . $option['value'] . '">' . $option['label'] .  '</option>';
                        $selected = ($answer && $option['value'] == $answer->answer) ? 'selected' : '';
                        $input .= '<option  value="' . $option['value'] . '" '. $selected . '>' . $option['label'] .  '</option>';


                    }
                    $input .= '</select> <span class="text-danger">( '. ( $answer ? $answer->points : '-' ) .' pt)</span>';
                    break;
                case 'qcm':
                    $options = json_decode($q->options,true);
                    $input = '';
                    foreach ($options as $option) {
                        $checked = ($answer && $option['value'] == $answer->answer) ? ' checked ' : '';
                        $input .= '<div class="form-check">
                            <input class="form-check-input disabled" ' . $checked . ' type="radio" disabled name="quiz-' . $q->id . '" " value="'. $option['value'] .'" />
                            <label class="form-check-label" > '. $option['label'] .'</label>
                        </div> ';
                    }
                    $input .= '<span class="text-danger">( '. ( $answer ? $answer->points : '-' ) .' pt)</span>';
                    break;
                case 'rl':
                    $input = '<textarea class="rl form-control" name="quiz-'. $q->id .'">' . ($answer ? $answer->answer : '') .'</textarea><div class="form-group"><label class="text-danger" for="">Points : </label><input type="number" name="point-'. $q->id .'" value="' . ($answer ? $answer->points : '')  . '" class="form-control cand-res" /></div></div>';
                    break;
                case 'rla':
                    $input = '<textarea class="form-control" disabled >' . $q->options .'</textarea>';
                    $input .= '<div><span class="text-danger">( '. ( $answer ? $answer->points : '-' ) .' pt)</span></div>';
                    break;

            }

            $body = str_replace('[*'. $q->id .']',$input,$body);
        }







        return view('dashboard.users.test',compact('user','test','body','detailed_points'));
    }


    public function updateTest(Request $request,$id,$test_id)
    {
        // GET QUIZES OF TYPE 'rl'
        $rls = Quiz::where('test_id',$test_id)->where('type','rl')->get();

        // GET PIVOT TABLE user->test
        $test_user = DB::table('test_user')->where('user_id',$id)->where('test_id',$test_id)->first();

        foreach ($rls as $rl) {
            DB::table('test_user_quiz')
                ->where('test_user_id',$test_user->id)
                ->where('quiz_id',$rl->id)
                ->update(['points' => request('point-'. $rl->id),'answer' => request('quiz-'. $rl->id)]);
        }

        return redirect()->route('users.tests.show',[$id,$test_id]);
    }

    public function resetTest($user_id,$test_id)
    {
        $test_user = \DB::table('test_user')->where('user_id',$user_id)->where('test_id',$test_id);
        $test_user->update(['completed' => false]);

        $test_user_id = $test_user->first()->id;
        \DB::table('test_user_quiz')->where('test_user_id',$test_user_id)->delete();

        return redirect()->back()->withSuccess(trans('app.success_update'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = User::findOrFail($id);
        return view('dashboard.users.edit',compact('item'));

    }

    public function report($id,$test_id)
    {
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load(storage_path("app/eval.xlsx"));

        $user = User::findOrFail($id);
        $test = Test::with('quizes')->findOrFail($test_id);

        $test_user = DB::table('test_user')->where('user_id',$id)->where('test_id',$test_id)->first();


        $answers = DB::table('test_user_quiz')->where('test_user_id',$test_user->id)->get();

        $test->quizes->groupBy('exercise_number')->each(function($v,$k) use ($answers,$spreadsheet) {
            //dump(["ex" => $k, "user_points" => $answers->whereIn('quiz_id',$v->pluck('id'))->sum('points')]);
            $column = null;
            $points = $answers->whereIn('quiz_id',$v->pluck('id'))->sum('points');
            switch ($k) {
                case 0:
                    $column = 'H7';
                    break;
                case 1:
                    $column = 'I7';
                    break;
                case 2:
                    $column = 'H8';
                    break;
                case 3:
                    $column = 'H9';
                    break;
                case 4:
                    $column = 'H10';
                    break;
                case 5:
                    $column = 'G11';
                    break;
                case 6:
                    $column = 'H11';
                    break;
                case 7:
                    $column = 'I11';
                    break;
                case 8:
                    $column = 'H12';
                    break;
                case 9:
                    $column = 'I13';
                    break;
                case 10:
                    $column = 'I14';
                    break;
                default:
                    return;
            }
            //dump(["k" => $k , "points" => $points, "column" => $column]);
            $spreadsheet->getActiveSheet()->setCellValue($column, $points);
        });

        $spreadsheet->getActiveSheet()->setCellValue('B3', $user->prenom.' '.$user->nom);
        $spreadsheet->getActiveSheet()->setCellValue('F3', $user->date);

        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xlsx");
        $writer->save(storage_path("app/eval2.xlsx"));
        return response()->download(storage_path("app/eval2.xlsx"))->deleteFileAfterSend(true);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, User::rules(true, $id));

        $item = User::findOrFail($id);
        $item->update($request->all());
        $item->tests()->sync($request->tests);

        return redirect()->route('users.index')->withSuccess(trans('app.success_update'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);

        return back()->withSuccess(trans('app.success_destroy'));

    }

}
