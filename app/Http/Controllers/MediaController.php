<?php

namespace App\Http\Controllers;

use App\Media;

use Illuminate\Http\Request;

class MediaController extends Controller
{
    public function index()
    {
        $items = Media::all();
        return view('dashboard.media.index',compact('items'));
    }


    public function store(Request $request)
    {
        $this->validate($request,Media::rules());

        Media::create($request->all());

        return redirect()->route('media.index')->withSuccess(trans('app.success_store'));
    }

    public function destroy($id)
    {
        Media::destroy($id);
        return back()->withSuccess(trans('app.success_destroy'));

    }
}
