<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Test;

class DashboardController extends Controller
{
    public function index()
    {
        $tests = Test::count();
        $users = User::count() - 1;
        return view('dashboard.home',compact('tests','users'));
    }
}
