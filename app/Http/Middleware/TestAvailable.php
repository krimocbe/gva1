<?php

namespace App\Http\Middleware;

use Closure;

class TestAvailable
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $test_id  = $request->route('id');
        $test_available = !auth()->user()->tests()->findOrFail($test_id)->pivot->completed;
        if($test_available) {
            return $next($request);
        } else {
            return redirect()->route('candidat.tests');
        }
    }
}
