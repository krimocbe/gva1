let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */


/*const babelOptions = global.options.babel;

if (!babelOptions.plugins) {
  babelOptions.plugins = [];
  babelOptions.plugins.push('transform-object-rest-spread');
  babelOptions.plugins.push('transform-class-properties');
}
*/

mix.webpackConfig(webpack => {
    return {
        plugins: [
            new webpack.ProvidePlugin({
                $: 'jquery',
                jQuery: 'jquery',
                'window.jQuery': 'jquery',
            })
        ]
    };
});

mix.js('resources/assets/js/app.js', 'public/js')
   .react('resources/assets/js/dashboard.js','public/js')
   .react('resources/assets/js/dashboard.test.js','public/js')
   .sass('resources/assets/sass/app.scss', 'public/css')
   .copyDirectory('resources/assets/static/images','public/images')
   .sourceMaps();
