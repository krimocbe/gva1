<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('nom');
            $table->string('prenom');
            $table->string('titre');
            $table->text('adresse')->nullable();
            $table->string('cp')->nullable();
            $table->string('ville')->nullable();
            $table->string('tel')->nullable();
            $table->integer('aff')->nullable();
            $table->string('conseiller')->nullable();
            $table->string('conseiller_tel')->nullable();
            $table->string('conseiller_email')->nullable();
            $table->date('date')->nullable();
            $table->date('debut_eval')->nullable();
            $table->date('fin_eval')->nullable();
            $table->integer('date_type')->nullable();
            $table->date('premier_entretien')->nullable();
            $table->string('objectif')->nullable();
            $table->string('consultant')->nullable();
            $table->text('remarques')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
