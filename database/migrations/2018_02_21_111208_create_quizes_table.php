<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuizesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quizes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type');
            $table->text('options')->nullable();
            $table->text('answer')->nullable();
            $table->integer('points')->nullable();
            $table->integer('test_id')->unsigned();

            $table->boolean('case_sensitive')->nullable()->default(false);
            $table->integer('exercise_number')->unsigned();
            $table->timestamps();


            $table->foreign('test_id')->references('id')->on('tests')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quizes');
    }
}
