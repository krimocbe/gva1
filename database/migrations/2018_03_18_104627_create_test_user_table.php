<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('test_user', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('test_id');
            $table->boolean('completed')->default(false);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('test_id')->references('id')->on('tests')->onDelete('cascade');
        });

        Schema::create('test_user_quiz', function (Blueprint $table) {
            $table->unsignedInteger('test_user_id');
            $table->unsignedInteger('quiz_id');
            $table->unsignedInteger('points')->default(0);
            $table->text('answer')->nullable();
            
            // To Do: add resul fields

            $table->foreign('test_user_id')->references('id')->on('test_user')->onDelete('cascade');
            $table->foreign('quiz_id')->references('id')->on('quizes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('test_user_quiz');
        Schema::dropIfExists('test_user');
    }
}
