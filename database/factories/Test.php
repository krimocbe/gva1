<?php

use Faker\Generator as Faker;

$factory->define(App\Test::class, function (Faker $faker) {
    return [
        "language" => rand(0,2),
        "name" => $faker->sentence(),
        "body_question" => $faker->randomHtml(2,3),
        "duration" => rand(5,100),
    ];
});
