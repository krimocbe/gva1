<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'nom' => $faker->lastName,
        'prenom'=> $faker->firstName,
        'titre'=> $faker->title,
        'email' => $faker->unique()->safeEmail,
        'password' => 'test', 
        'remember_token' => str_random(10),
    ];
});
