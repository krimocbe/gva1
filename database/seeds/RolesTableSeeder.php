<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new Role();
        $admin->name         = 'admin';
        $admin->display_name = 'User Administrator';
        $admin->description  = 'User is allowed to manage and edit everything';
        $admin->save();


        $candidat = new Role();
        $candidat->name         = 'candidat';
        $candidat->display_name = 'candidat';
        $candidat->description  = 'candidat who will pass the exam';
        $candidat->save();




    }
}
