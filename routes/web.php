<?php

Auth::routes();

Route::get('/', function() {
    return redirect()->to('/candidat');
});

Route::group([ 'prefix' => 'dashboard','middleware' =>['auth']],function(){

  Route::get('/','DashboardController@index');
  Route::resource('tests', 'TestsController');
  Route::group(['prefix' => 'tests/{test}'], function() {
    Route::resource('quiz', 'QuizesController');
  });


  Route::group(['prefix' => 'users/{id}/tests'], function() {
      Route::get('/','UsersController@tests')->name('users.tests');
      Route::get('/{test_id}','UsersController@test')->name('users.tests.show');
      Route::post('/{test_id}','UsersController@updateTest')->name('users.tests.update');
      Route::put('/{test_id}','UsersController@resetTest')->name('users.tests.reset');
      Route::get('/{test_id}/report','UsersController@report')->name('users.tests.report');
  });
  Route::resource('users','UsersController');

  Route::group(['prefix' => 'media'], function() {
    Route::get('/','MediaController@index')->name('media.index');
    Route::post('/','MediaController@store')->name('media.store');
    Route::delete('/{id}','MediaController@destroy')->name('media.destroy');
  });


});


Route::group(['prefix' => 'candidat'], function()
{
  Route::get('login','CandidatController@login')->name('candidat.login');
  Route::post('login','CandidatController@handleLogin')->name('candidat.login');
  Route::group(['middleware' => ['auth']], function() {
    Route::get('tests','CandidatController@tests');
    Route::get('/','CandidatController@tests')->name('candidat.tests');

    Route::get('test/{id}/end','CandidatController@test_end')->name('candidat.test_end');
    Route::get('test/{id}','CandidatController@test')->name('candidat.test')->middleware('tavailable');
    Route::post('test/{id}','CandidatController@postTest');
  });

});
