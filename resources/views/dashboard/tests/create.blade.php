@extends('dashboard.layouts.main')

@section('page-header')
    Tests <small>{{ trans('app.add_new_item') }}</small>
@endsection

@section('content')

    {!! Form::open([
            'action' => ['TestsController@store']
        ])
    !!}

    @include('dashboard.tests.form')

    <button type="submit" class="btn btn-info">Sauvegarder</button>

    {!! Form::close() !!}

@endsection
