@extends('dashboard.layouts.main')

@section('page-header')
    Tests <small>{{ trans('app.update_item') }}</small>
@endsection

@section('content')
    <div>
        {!! Form::model($test, ['method' => 'PATCH','route' => ['tests.update', $test->id]]) !!}
        <div class="bgc-white p-20 mB-20 bd">
          <h6 class="c-grey-900">Test</h6>

          <div class="m-20">

            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="inputNom">Nom</label>
                <input id="inputNom" type="text" value="{{ $test->name }}" class="form-control" name="name" />
              </div>
              <div class="form-group col-md-6">
                <label for="inputExercises">Nombre d'exercices</label>
                <input id="inputExercises" type="number" name="num_exercises" value="{{ $test->num_exercises }}" class="form-control"/>
              </div>
            </div>

            <div class="form-row">
              <div class="form-group col-md-6">
                    {!! Form::mySelect('language', 'Language', config('gva.languages')) !!}
              </div>
              <div class="form-group col-md-6">
                <label for="inputDuration">Durée</label>
                <input id="inputDuration" type="number" name="duration" value="{{ $test->duration }}" class="form-control"/>
              </div>
            </div>

            <div class="form-group">
              <label>Corp du test</label>
                <textarea id="editor" name="body_question">{!! $test->body_question !!}</textarea>
            </div>


            <div class="row justify-content-end">
              <div class="col-md-3 d-flex justify-content-end">
                <button class="btn btn-primary btn-lg"><i class="ti-save"></i> Sauvegarder</button>
              </div>
            </div>
          </div>
        </div>
        {!! Form::close() !!}


        <div id="test-root" data-id="{{ $test->id }}"></div>
    </div>


@endsection

@section('css')
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/Ladda/1.0.0/ladda.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jodit/3.1.87/jodit.min.css">

@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jodit/3.1.87/jodit.min.js"></script>

    <script type="text/javascript">
        Jodit.modules.Audio = function (editor) {
          this.insertAudioTag = function (w, h, textcolor, bgcolor) {
              var image = Jodit.modules.Dom.create('audio', '', editor.ownerDocument);
              image.setAttribute('controls',true)
              image.setAttribute('src', 'http://localhost:8000/storage/media/media_5ad253ea36a467.20250074.mpga');
              editor.selection.insertNode(image);
              editor.setEditorValue(); // for syncronize value between source textarea and editor
          };
        };
        var editor = new Jodit('#editor', {
            disabledPlugins: [
              "about"
            ],
            extraButtons: [
              {
                  name: 'audio',
                  iconURL: '/images/speaker.png',
                  tooltip: 'insert Audio Tag',
                  exec: function (editor) {
                      let src = prompt('Coller URL')
                      if(src) {
                        editor.dummy.insertDummyImage(src);
                      }
                  }
              }],
            events: {
                afterInit: function (editor) {
                    editor.audio = new Jodit.modules.Audio(editor);
                },
                getIcon: function (name, control, clearName) {
                   var code = clearName;
                   switch (clearName) {
                      case 'audio':
                           code = 'volume-up';
                           break;
                      case 'redo':
                       code = 'rotate-right';
                       break;
                     case 'video':
                         code = 'video-camera';
                         break;
                     case 'copyformat':
                         code = 'clone';
                         break;
                     case 'about':
                         code = 'question';
                         break;
                     case 'selectall':
                         code = 'legal';
                         break;
                     case 'symbol':
                         return '<span style="text-align: center;font-size:14px;">Î©</span>';
                     case 'hr':
                         code = 'minus';
                         break;
                     case 'left':
                     case 'right':
                     case 'justify':
                     case 'center':
                         code = 'align-' + name;
                         break;
                     case 'brush':
                         code = 'tint';
                         break;
                     case 'fontsize':
                         code = 'text-height';
                         break;
                     case 'ul':
                     case 'ol':
                         code = 'list-' + name;
                         break;
                     case 'source':
                         code = 'code';
                         break;
                   }
                   return '<i style="font-size:14px" class="fa fa-' + code + ' fa-xs"></i>';
                }
              }
        });

    </script>
    <script type="text/javascript" src="{{ mix("js/dashboard.test.js") }}"></script>
@endsection




