@extends('dashboard.layouts.main')

@section('title')
    Tests
@endsection

@section('content')

    <div class="d-flex mB-30">
        <h4 class="mr-auto c-grey-900">
        <span class="icon-holder">
            <i class="c-red-500 ti-file"></i>
        </span>
            Tests
        </h4>
        <a href="{{ route('tests.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Ajouter</a>
    </div>
    <table class="table">
        <thead class="thead-light">
        <tr>
            <th>Langue</th>
            <th>Test</th>
            <th>Durée</th>
            <th>Actions</th>
        </tr>
        </thead>

        <tbody>
        @foreach($tests as $test)
            <tr>
                <td>{{ $test->languageCode() }}</td>
                <td>{{ $test->name }}</td>
                <td>{{ $test->duration }}</td>
                <td>
                    <a href="{{ route('tests.edit',$test->id) }}" class="btn btn-primary btn-xs"><i
                                class="fa fa-pencil"></i></a>
                    {!! Form::open(['method' => 'DELETE','route' => ['tests.destroy', $test->id],'style'=>'display:inline']) !!}

                    <button type="submit" class="btn btn-danger btn-xs cur-p"><i class="fa fa-trash"></i></button>

                    {!! Form::close() !!}
                </td>

            </tr>
        @endforeach
        </tbody>
    </table>
@endsection


