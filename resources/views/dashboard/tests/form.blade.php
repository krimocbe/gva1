<div class="row mB-40">
  <div class="col-sm-8">
    <div class="bgc-white p-20 bd">

      {!! Form::myInput('text', 'name','Nom du test') !!}
      {!! Form::myInput('number', 'duration','Durée') !!}
      {!! Form::myInput('number', 'num_exercises',"Nombre d'exercices") !!}
      {!! Form::mySelect('language','Langue',["Francais","Anglais","Allemand"]) !!}
    </div>
  </div>
</div>

