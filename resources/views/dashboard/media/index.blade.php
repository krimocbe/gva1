@extends('dashboard.layouts.main')

@section('title')
Media
@endsection

@section('page-header')
    Media <small>{{ trans('app.manage') }}</small>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-4">
          <div class="bgc-white bd bdrs-3 p-20 mB-20">
            <h5 class="c-grey-900 mT-10 mB-30">Ajouter</h5>
            {!!
                Form::open([
                    'action' => ['MediaController@store'],
                    'files' => true
                ])
            !!}

            {!! Form::myInput('text', 'title', 'Titre', ['required']) !!}
            {!! Form::myFile('file', 'Fichier', ['required']) !!}


            <button type="submit" class="btn btn-primary">Ajouter</button>

            {!! Form::close() !!}
          </div>
        </div>
        <div class="col-md-8">
          <div class="bgc-white bd bdrs-3 p-20 mB-20">
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Titre</th>
                            <th>Actions</th>
                        </tr>
                    </thead>

                    <tbody>
                    @foreach($items as $item)
                        <tr>
                            <td>{{ $item->id }}</td>
                            <td>{{ $item->title }}</td>
                            <td>
                                <ul class="list-inline">
                                    <li class="list-inline-item">
                                        <input type="hidden" value="{{ $item->file }}" class="hidden-link">
                                        <button class="cur-p btn btn-primary btn-xs copy-link"><i class="fa fa-copy"></i></button>
                                    </li>
                                    <li class="list-inline-item">
                                        {!! Form::open([
                                            'method' => 'DELETE',
                                            'route' => ['media.destroy', $item->id],
                                            'class'=>'delete',
                                            ]) !!}

                                        <button type="submit" class="cur-p btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>

                                        {!! Form::close() !!}
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('js')
<script>
    $(function() {
        $('.copy-link').click(function() {
            alert($(this).prev().attr('value'));
        })
    })
</script>
@endsection

