@extends('dashboard.layouts.main')

@section('page-header')
    Candidat <small>{{ trans('app.add_new_item') }}</small>
@endsection

@section('content')

    {!! Form::open([
            'action' => ['UsersController@store']
        ])
    !!}

    @include('dashboard.users.form')

    <button type="submit" class="btn btn-info">{{ trans('app.add_button') }}</button>

    {!! Form::close() !!}
@endsection
