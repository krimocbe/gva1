@extends('dashboard.layouts.main')

@section('page-header')
    Candidat <small>{{ trans('app.update_item') }}</small>
@endsection

@section('content')

    {!! Form::model($item, ['method' => 'PATCH','route' => ['users.update', $item->id]]) !!}

        @include('dashboard.users.form')

        <button type="submit" class="btn btn-info">{{ trans('app.edit_button') }}</button>

    {!! Form::close() !!}
    
@endsection



