
<div class="row mB-40">
  <div class="col-sm-8">
    <div class="bgc-white p-20 bd">

        {!! Form::myInput('text', 'titre', 'Titre', ['required']) !!}

        {!! Form::myInput('text', 'nom', 'Nom', ['required']) !!}

        {!! Form::myInput('text', 'prenom', 'Prenom', ['required']) !!}

        {!! Form::myTextArea('adresse', 'Adresse', ['required']) !!}

        {!! Form::myInput('text', 'cp', 'CP') !!}

        {!! Form::myInput('text', 'ville', 'Ville') !!}

        {!! Form::myInput('text', 'tel', 'Telephone') !!}

        {!! Form::myInput('text', 'email', 'Email', ['required']) !!}

        {!! Form::myInput('password', 'password', 'Mot de passe') !!}
        {!! Form::myInput('password', 'password_confirmation', 'Confirmation Mot de passe') !!}

        {!! Form::mySelect('aff', 'aff', config('variables.users_aff')) !!}

        {!! Form::myInput('text', 'conseiller', 'Conseiller' ) !!}

        {!! Form::myInput('text', 'conseiller_tel', 'Conseiller Tel' ) !!}

        {!! Form::myInput('email', 'conseiller_email', 'Conseiller Email' ) !!}

        {!! Form::myInput('date', 'date', 'Date' ) !!}

        {!! Form::myInput('date', 'debut_eval', 'Date de l \'évaluation' ) !!}

        {!! Form::myInput('date', 'fin_eval', 'Au' ) !!}

        {!! Form::mySelect('date_type', 'Type', config('variables.users_date_type')) !!}

        {!! Form::myInput('date', 'premier_entretien', 'Date 1er entretien' ) !!}

        {!! Form::myInput('text', 'objectif', 'Objectif' ) !!}

        {!! Form::myInput('text', 'consultant', 'Consultant' ) !!}

        {!! Form::myTextArea('remarques', 'Remarques' ) !!}
    </div>
  </div>
  <div class="col-sm-4">
    <div class="bgc-white p-20 bd">
      {!! Form::mySelect('tests[]', 'Tests', App\Test::pluck('name', 'id')->toArray(), null, ['class' => 'form-control select2', 'multiple']) !!}
    </div>
  </div>
</div>







