@extends('dashboard.layouts.main')

@section('title')
Candidats
@endsection


@section('content')

    <div class="d-flex mB-30">
        <h4 class="mr-auto c-grey-900">
        <span class="icon-holder">
            <i class="c-green-500 ti-user"></i>
        </span>
            Candidats
        </h4>
        <a href="{{ route('users.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Ajouter</a>
    </div>

	<div class="row">
		<div class="col-md-12">
		  <div class="bgc-white bd bdrs-3 p-20 mB-20">
				<table class="table table-striped table-bordered">
					<thead>
						<tr>
							<th>Nom</th>
							<th>Email</th>
							<th>Resultats</th>
							<th>Actions</th>
						</tr>
					</thead>

					<tbody>
					@foreach($items as $item)
						<tr>
							<td><a href="{{ route('users.edit',$item->id) }}">{{ $item->nom }} {{ $item->prenom }}</a></td>
							<td><a href="{{ route('users.edit',$item->id) }}">{{ $item->email }}</a></td>
							<td><a href="{{ route('users.tests',$item->id) }}" class="btn btn-primary btn-sm cur-p"><i class="fa fa-check"></i></button></td>
							<td>
								<ul class="list-inline">
									<li class="list-inline-item">
										<a href="{{ route('users.edit',$item->id) }}" class="btn btn-sm btn-primary"><i
											class="fa fa-pencil"></i></a>
									</li>
									<li class="list-inline-item">
										{!! Form::open([
											'method' => 'DELETE',
											'route' => ['users.destroy', $item->id],
											'class'=>'delete',
											]) !!}

										<button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>

										{!! Form::close() !!}
									</li>
								</ul>


							</td>
						</tr>
					@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection

