@extends('dashboard.layouts.main')

@section('page-header')
    Test - {{ $test->name }}
@endsection

@section('content')
    <form method="POST" action="{{ route('users.tests.update',[$user->id,$test->id]) }}">
        <div class="row">
            {{ csrf_field() }}
            <div class="col-md-12">
                <div class="bgc-white bd bdrs-3 p-20 mB-20">
                    {!! $body !!}
                </div>
                <div class="d-flex  mB-10 justify-content-end" style="width: 100%">
                   <button type="submit" class="cur-p btn btn-primary btn-md mR-10 "><i class="fa fa-refresh"></i> Mettre à jour</button>
                   <a href="{{ route('users.tests.report',[$user->id,$test->id]) }}" class="cur-p btn btn-primary btn-md"><i class="fa fa-download"></i> Telecharger Rapport</a>
                </div>
                <div class="bgc-white p-20 bd" style="width: 100%">
                    <table class="table">
                        <thead>
                            <tr>
                                <th></th>
                                <th class="text-right">Points</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($detailed_points->count())
                            @for ($i = 0; $i < $detailed_points->count(); $i++)
                                <tr>
                                <td>Exercice {{$i+1}}</td>
                                <td class="text-right">{{ $detailed_points[$i]['user_points'] }}/{{ $detailed_points[$i]['real_points'] }}</td>
                                </tr>
                            @endfor
                            @endif
                            <tr>
                                <td><b>Total</b></td>
                                <td class="text-right"><b>{{ $detailed_points->sum('user_points') }}/{{ $detailed_points->sum('real_points') }}</b></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('css')
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/Ladda/1.0.0/ladda.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jodit/3.1.87/jodit.min.css">
@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jodit/3.1.87/jodit.min.js"></script>

    <script type="text/javascript">
        $('textarea.rl').each(function (elm) {
            var editor = new Jodit(this);
        });
    </script>

@endsection

