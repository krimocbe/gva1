@extends('dashboard.layouts.main')
@section('title')
    Tests
@endsection
@section('page-header')
    Tests
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="bgc-white bd bdrs-3 p-20 mB-20">
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th style="width:85%">Nom</th>
                            <th style="text-align: center;">Validé</th>
                            <th style="text-align: center;">Details</th>
                            <th style="text-align: center;">Réinitialiser</th>
                        </tr>
                    </thead>

                    <tbody>
                    @foreach($user->tests as $test)
                        <tr>
                            <td>{{ $test->name }}</td>
                            <td style="text-align: center;">
                                @if($test->pivot->completed)
                                    <i class="fa fa-check fa-2x" style="color: green"></i>
                                @else
                                    <i class="fa fa-times fa-2x" style="color: red"></i>
                                @endif
                            </td>
                            <td style="text-align: center;"><a href="{{ route('users.tests.show',[$user->id,$test->id]) }}" class="btn btn-primary btn-sm"><i class="fa fa-search"></i></a></td>
                            <td style="text-align: center;">
                                        {!! Form::open([
                                            'method' => 'PUT',
                                            'route' => ['users.tests.reset', $user->id,$test->id],
                                            'class'=>'delete',
                                            ]) !!}

                                        <button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-repeat"></i></button>

                                        {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
    </div>
@endsection

