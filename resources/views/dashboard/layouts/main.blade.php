<!DOCTYPE html>
<html>

<head>
    <title>GVA Dashboard - @yield('title')</title>
    <link rel="stylesheet" href="/css/app.css">
    @yield('css')
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>

<body class="app">
<!-- @TOC -->
<!-- =================================================== -->
<!--
  + @Page Loader
  + @App Content
      - #Left Sidebar
          > $Sidebar Header
          > $Sidebar Menu

      - #Main
          > $Topbar
          > $App Screen Content
-->


@include('dashboard.partials.spinner')

<!-- @App Content -->
<!-- =================================================== -->
<div>
    <!-- #Left Sidebar ==================== -->
@include('dashboard.partials.sidebar')

<!-- #Main ============================ -->
    <div class="page-container">
        <!-- ### $Topbar ### -->
    @include('dashboard.partials.topbar')

    <!-- ### $App Screen Content ### -->
        <main class='main-content bgc-grey-100'>
            <div id='mainContent'>
              <div class="container-fluid">

                <h4 class="c-grey-900 mT-10 mB-30">@yield('page-header')</h4>

                @include('dashboard.partials.flash')
                @yield('content')
              </div>
            </div>
        </main>

        <!-- ### $App Screen Footer ### -->
        <footer class="bdT ta-c p-30 lh-0 fsz-sm c-grey-600">
				<span>Copyright © 2017 by
					<a href="https://digitalex.dz" target='_blank' title="DigitalEx">DigitalEx</a>. All rights reserved.</span>
        </footer>
    </div>
</div>

<script type="text/javascript" src="/js/app.js"></script> </body>
@yield('js')

</html>
