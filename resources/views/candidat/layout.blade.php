<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8"/>
    <title>GVA</title>
{{--     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/1.11.8/semantic.min.css"/> --}}
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500" rel="stylesheet">
    <style>
        html,.main {
            height: 100%;
            background-color: white;
        }
        body {
            font-family: 'Roboto', sans-serif;

        }
        .gva-q {
            margin-bottom: 15px;
        }

        .gva-q .ui.input input {
            padding: .2em 0.2em;
        }
        .badge-gva {
            background-color: #B8B5D4;
            color: white;

        }


    </style>
    @yield('css')

</head>
<body>
@yield('content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://unpkg.com/sweetalert2@7.19.3/dist/sweetalert2.all.js"></script>

<script>
    // $(function() {
    //     $('select.dropdown').dropdown();
    //     $('.ui.radio.checkbox').checkbox();
    //     $(".input").prev("p").css("display", "inline");
    // })
</script>
@yield('js')
</body>
</html>
