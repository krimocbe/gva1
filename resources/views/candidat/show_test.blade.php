@extends('candidat.layout')

@section('content')
    <div class="main" style="">
        <header class="mb-2 p-2 test-header">
            <div class="row">
                <div class="col-md-1">
                    <img src="/images/gva_logo_small.png" class="img-fluid" style="max-height: 100px" alt="" >
                </div>
                <div class="col-md-9">
                    <h1>{{ $test->name }}</h1>
                    <small>Durée indicative : {{ $test->duration }} minutes</small>
                    <br>
                    <small>{{ date('d/m/Y') }}</small>
                </div>
                <div class="col-md-2">
                    <div class="p-2">
                      <h5>Bonjour {{ $user->titre }} {{ $user->nom }} {{ $user->prenom }}</h5>
                      <div class="d-flex">
                      <a class="btn btn-primary btn-sm" style="flex: 1; margin-right: 5px;" href="{{ route('candidat.tests') }}"><i class="fa fa-arrow-left"></i> Retour</a>
                      {!! Form::open([
                          'method' => 'POST',
                          'route' => ['logout'],
                          'style' => 'display: inline'
                      ]) !!}

                      <button class="btn btn-secondary btn-sm"><i class="fa fa-sign-out"></i> Se Déconnecter</button>

                      {!! Form::close() !!}
                      </div>

                      <button class="btn btn-validate btn-success btn-block btn-md rounded-0 mt-1"> <i class="fa fa-check"></i> Valider</button>
                    </div>

                </div>
            </div>

            <div style="float: right">

            </div>
        </header>
            <form method="POST" class="test-form" action="{{ url()->current() }}" >
            {{ csrf_field() }}

            <div class="row justify-content-md-center clearfix " >
                <div class="col-md-10" >
                    <div class="exbar d-flex justify-content-between">
                        <button type="button" class="btn btn-sm btn-primary rounded-0 btn-prev">Précédent</button>
                        <div class="btn-group ex-group" role="group" aria-label="Button group with nested dropdown">
                        </div>
                        <button type="button" class="btn btn-sm btn-primary rounded-0  btn-next">Suivant</button>
                    </div>
                    <div class="paper" style="padding: 30px 15px">{!! $body !!}</div>
                </div>
            </div>

            <footer class="row justify-content-md-center mt-5">

            </footer>
        </form>

    </div>






@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jodit/3.1.87/jodit.min.js"></script>

    <script type="text/javascript">
        $(document).on('click', ".btn-validate", function(e) {
            var _this = $(this);
            e.preventDefault();
            swal({
                title: 'Valider', // Opération Dangereuse
                text: 'Êtes-vous sûr de vouloir valider le test ?', // Êtes-vous sûr de continuer ?
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: 'null',
                cancelButtonColor: 'null',
                confirmButtonClass: 'btn btn-danger',
                cancelButtonClass: 'btn btn-primary',
                confirmButtonText: 'Oui, sûr!', // Oui, sûr
                cancelButtonText: 'Annuler', // Annuler
                closeOnConfirm: false
            }).then(res => {
                if (res.value) {
                    $('.test-form').submit();
                }
            });
        });
        $('textarea.rl').each(function (elm) {
            var editor = new Jodit(this);
        });
        var ex_count = $('.exercice').size()
        var current_id = 1;

        $(function() {
            $('.exercice').each(function(k,v) {
                $(this).attr('data-id',k+1)
                $('.ex-group').append('<button type="button" data-id="' + (k+1) + '"" class="btn btn-sm btn-outline btn-ex">EX ' + (k+1) + '</button>')
            })

        $('.btn-prev').attr('disabled',true)
        $('.btn-ex:first').removeClass('btn-outline').addClass('btn-success')
        $('.exercice:first').removeClass('hidden').addClass('active')

        $('.btn-prev').click(function() {
            current_id--;
            $('.btn-next').removeAttr('disabled')
            $('.btn-ex').removeClass('btn-success').addClass('btn-outline')
            $('.btn-ex[data-id="' + current_id + '"]').removeClass('btn-outline').addClass('btn-success')
            $('.exercice').addClass('hidden')
            $('.exercice[data-id="' + current_id + '"]').removeClass('hidden')
            if(current_id === 1) {
                $(this).attr('disabled',true)
            }
        })
        $('.btn-next').click(function() {
            $('.btn-prev').removeAttr('disabled')
            $('.exercice').addClass('hidden')
            current_id++;
            $('.btn-ex').removeClass('btn-success').addClass('btn-outline')
            $('.btn-ex[data-id="' + current_id + '"]').removeClass('btn-outline').addClass('btn-success')
            $('.exercice[data-id="' + current_id + '"]').removeClass('hidden')
            if(current_id === ex_count) {
                $(this).attr('disabled',true)
                return
            }
        })
        $('.btn-ex').click(function() {
            $('.btn-ex').removeClass('btn-success').addClass('btn-outline')
            $(this).removeClass('btn-outline').addClass('btn-success')
            $('.exercice').addClass('hidden')
            current_id = parseInt($(this).attr('data-id'));
            $('.exercice[data-id="' + $(this).attr('data-id') + '"]').removeClass('hidden')
            if(current_id === 1) {
                $('.btn-prev').attr('disabled',true)
                $('.btn-next').removeAttr('disabled')
            }
            if(current_id === ex_count) {
                $('.btn-next').attr('disabled',true)
                $('.btn-prev').removeAttr('disabled')

            }

        })
        })


    </script>

@endsection

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jodit/3.1.87/jodit.min.css">
    <style>
        body {
            background-color : #EEE;
        }
        .table-bordered , .table-bordered td, .table-bordered th {
            border: 1px solid black;
        }
        .test-header {
            border-bottom: 26px solid #736BA6;
            background-color: white;
        }
        .paper {
          background: #fff;
          box-shadow:
            /* The top layer shadow */
            0 1px 1px rgba(0,0,0,0.15),
            /* The second layer */
            0 10px 0 -5px #eee,
            /* The second layer shadow */
            0 10px 1px -4px rgba(0,0,0,0.15),
             /* The third layer */
            0 20px 0 -10px #eee,
            /* The third layer shadow */
            0 20px 1px -9px rgba(0,0,0,0.15);
            /* Padding for demo purposes */
            padding: 30px;
        }

        .btn-ex {
            border-radius: 0;
        }

        .hidden {
            display: none;
        }

        input.form-control, select.form-control {
            width: auto;
            display: inline;
        }


    </style>
@endsection
