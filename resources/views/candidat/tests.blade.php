@extends('candidat.layout')

@section('content')
<div class="jumbotron jumbotron-fluid" style="background-color: #736BA6; color: white;">
  <div class="container">
    <h1 class="display-4">Bonjour, {{ auth()->user()->titre }} {{ auth()->user()->nom }} {{ auth()->user()->prenom }}</h1>
    <p class="lead">Bienvenue sur la plateforme de test en ligne de GVA Ressources.<br /> Nous vous invitons, en préambule à prendre connaissance des informations de navigation énoncées ci-dessous.<br />Ensuite, pour démarrer le test, cliquez sur le bouton &#9658; Commencer</p>
  </div>
</div>



    <div class="row justify-content-md-center" style="margin-top: 50px;">
        <div class="col-md-6">
            @if (\Session::get('success') == true)
                <div class="alert alert-success mb-2">
                  <h5>Votre module est validé.</h5>
                  <p>Nous vous remercions d'avoir effectué ce module. Nous vous invitons à vous annoncer à la réception de GVA Ressources pour la suite de votre Evaluation Commerciale et Bureautique.</p>
                </div>
            @endif
            <h3 class="">Vos modules de tests</h3>
            <div>
                @foreach($tests as $test)
                    <div class="media mb-2 border-bottom border-secondary p-1 ">
                        <div class="media-body ">
                            <span class="badge badge-pill badge-gva mr-3">{{ $test->languageCode() }}</span>
                            <span> {{ $test->name }}</span>
                        </div>
                        @if(!$test->pivot->completed)
                            <a class="btn btn-primary cur-p" href={{ route('candidat.test',$test->id) }}><i class="fa fa-play"></i> Commencer</a>
                        @else
                            <button type="button" class="btn btn-secondary cur-p" disabled><i class="fa fa-check"></i> Validé</button>
                        @endif
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    <div class="row justify-content-md-center" style="margin-top: 50px;">
        <div class="col-md-10">
          <h4 style="margin-left: 15%;">Important: Instructions de navigation</h4>
          <img src="{{ asset('images/gva_help_text.jpg') }}" alt="" class="img-fluid">
        </div>
    </div>
@endsection

@section('js')

    <script type="text/javascript">
        $(window).on('load',function(){
            $('#myModal').modal('show');
        });
    </script>

@endsection
