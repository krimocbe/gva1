import axios from 'axios';

export const addQuiz = (test_id,quiz) =>
  axios({
    method: 'POST',
    url: `/dashboard/tests/${test_id}/quiz`,
    data: quiz
  });


export const deleteQuiz = (id,quiz_id) =>
  axios({
    method: 'DELETE',
    url: `/dashboard/tests/${id}/quiz/${quiz_id}`,
  });


export const saveQuiz = (test_id,quiz) =>
  axios({
    method: 'PUT',
    url: `/dashboard/tests/${test_id}/quiz/${quiz.id}`,
    data: quiz
  });
