import React, { Component } from 'react';
import ReactModal from 'react-modal';
import Prod from '../../../../shared/Prod';
import { UpdateTest } from '../../../../actions/test';
import { addopquizz } from '../../../../actions/quizz';


const style = {
  overlay: {
    zIndex: 4000,
    position: 'fixed',
    left: 0,
    top: 0,
    width: '100%',
    height: '100%',
    overflow: 'auto',
    backgroundColor: 'rgba(0,0,0,0.4)'
  },
  content: {
    overflow: 'auto',
    WebkitOverflowScrolling: 'touch',
    backgroundClip: 'padding-box',
    backgroundColor: '#FFFFFF',
    border: '1px solid rgba(0, 0, 0, 0)',
    borderRadius: '4px',
    boxShadow: '0 1px 3px rgba(0, 0, 0, 0.3)',
    outline: '0 none',
    width: '80%',
    margin: '10% auto'
  }
};


export default class TestModal extends Component {
  constructor (props) {
    super(props);
    this.state = {
      quizz: {
        quiz_body: '',
        id_test: this.props.id,
        point: 0,
        quiz_type: 'PROD'
      },
      quizzes: []
    };

    this.handlesubmit = this.handlesubmit.bind(this);

    this.handleInput = this.handleInput.bind(this);


  }

  handleInput (key, e) {

    /*Duplicating and updating the state */
    var state = Object.assign({}, this.state.quizz);
    console.log('avant');
    console.log(state);
    state[key] = e.target.value;
    console.log('apres');
    console.log(state);
    this.setState({
      quizz: state
    });

    console.log(this.state.quizz);

  }

  handlesubmit () {

    var state = Object.assign({}, this.state.quizz);
    console.log(state);
    addopquizz(state).then((data) => {
      console.log(data);
    });
  }

  render () {
    console.log(this.props.currentquizz);
    return (
      <ReactModal
        isOpen={this.props.modalShown}
        contentLabel="Upload Audio Word"
        style={style}
        shouldCloseOnOverlayClick={true}
        className="modal-content"
        onOk={this.props.onCreate}

      >
        <div className="modal-header">
          <button type="button" className="close" onClick={() => this.props.handleClose()}><span
            aria-hidden="true">×</span><span className="sr-only">Close</span></button>
          <h4 className="modal-title">Test</h4>
        </div>
        <div className="modal-body">
          <h1>TEST</h1>
          <div className="row">
            <div className="col-md-12">

              <form>


                <div className="form-group">
                  <label className="col-md-4 control-label">Corp question</label>

                  <div className="col-md-6">
                    <textarea id="email" type="text" className="form-control" 
                              onChange={(e) => this.handleInput('quiz_body', e)}/>
                  </div>
                </div>
                <div className="form-group">
                  <label className="col-md-4 control-label">point</label>

                  <div className="col-md-6">
                    <input type="number" className="form-control" onChange={(e) => this.handleInput('point', e)}/>
                  </div>
                </div>


              </form>


            </div>
          </div>
        </div>

        <div className="modal-footer">
          <button type="button" className="btn btn-default" onClick={(e) => this.props.handleClose()}>Close</button>
          <button className="btn btn-primary" onClick={() => this.props.handleSave(this.state.quizz)}>Save</button>

        </div>


      </ReactModal>
    );
  }
}


